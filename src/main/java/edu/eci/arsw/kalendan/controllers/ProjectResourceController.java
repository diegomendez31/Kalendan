
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.kalendan.controllers;

import ch.qos.logback.core.CoreConstants;
import edu.eci.arsw.kalendan.model.Activity;
import edu.eci.arsw.kalendan.model.Project;
import edu.eci.arsw.kalendan.model.User;
import edu.eci.arsw.kalendan.services.ProjectService;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Diego
 */
@RestController
@RequestMapping(value = "/kalendan")
public class ProjectResourceController {
    
    @Autowired
    ProjectService pj=null;
    
    @RequestMapping(method = RequestMethod.GET)
    public String test(){
        return "Oki";
    }
    
    @RequestMapping(path = "/projects/calendar/{username}", method = RequestMethod.GET)
    public ResponseEntity<?> projectGetRecursoProyectosUsuario(@PathVariable String username){
    try {
        return new ResponseEntity<>(pj.getProjectsUsuario(username),HttpStatus.ACCEPTED);
    } catch (Exception ex) {
        Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
        return new ResponseEntity<>("No encontro Datos!",HttpStatus.NOT_FOUND);
    }    
    }

    @RequestMapping(path = "/projects/{projectid}/{username}", method = RequestMethod.GET)
    public ResponseEntity<?> projectGetRecursoActividades(@PathVariable Integer projectid, @PathVariable String username){
    try {

        return new ResponseEntity<>(pj.getActividadesProject(projectid, username),HttpStatus.ACCEPTED);
    } catch (Exception ex) {
        Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
        return new ResponseEntity<>("No encontro Datos!",HttpStatus.NOT_FOUND);
    }    
    }
    
    @RequestMapping(path = "/actividades/{date}", method = RequestMethod.POST)
    public ResponseEntity<?> getActivitiesUserDate(@RequestBody String user, @PathVariable("date") String date){
    try {      
        Date fecha = new Date(Long.parseLong(date));

        return new ResponseEntity<>(pj.getUserDateActivities(user, fecha),HttpStatus.ACCEPTED);
    } catch (Exception ex) {
        Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
        return new ResponseEntity<>("No encontro Datos!",HttpStatus.NOT_FOUND);
    }    
    }
    
    
    @RequestMapping(path = "/actividades/{projectid}/{nomUser}", method = RequestMethod.PUT)
    public ResponseEntity<?> manejadorPutRecursoActividad(@RequestBody Activity a, @PathVariable Integer projectid, @PathVariable String nomUser){
        try {
            
            pj.registrarActividad(a, projectid, nomUser);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception ex) {
            Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(ex.getMessage(),HttpStatus.NOT_FOUND);
        }
    }
    
   @RequestMapping(path = "/equipos/{idProyecto}", method = RequestMethod.GET)
    public ResponseEntity<?> projectGetRecursoEquipo(@PathVariable Integer idProyecto){
    try {        
        return new ResponseEntity<>(pj.cargarUsers(idProyecto),HttpStatus.ACCEPTED);
    } catch (Exception ex) {
        Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
        return new ResponseEntity<>("No encontro Equipo!",HttpStatus.NOT_FOUND);
    }   
    
    }
    
    @RequestMapping(path = "/actividades/calendario/{projectid}/{nomUser}/{idActivity}", method = RequestMethod.PUT)
    public ResponseEntity<?> manejadorPutRecursoFechaActividad(@RequestBody Date d, @PathVariable Integer projectid, @PathVariable String nomUser, @PathVariable Integer idActivity){
        try {
            pj.cambiarFechaActividad(d, projectid, nomUser, idActivity);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception ex) {
            Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(ex.getMessage(),HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(path = "/projects/{projectid}", method = RequestMethod.GET)
    public ResponseEntity<?> projectGetName(@PathVariable Integer projectid){
        try{
            
            return new ResponseEntity<>(pj.getNameProject(projectid),HttpStatus.ACCEPTED);
        } catch(Exception ex){
            Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(path = "/equipos/{idT}/{permiso}", method = RequestMethod.POST)
    public ResponseEntity<?> teadmAddMiembros(@PathVariable("idT") Integer teamId, @PathVariable String permiso, @RequestBody String u){
        try{
            pj.agregarMiembro(u, teamId, permiso);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch(Exception ex){
            Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }
    
    @RequestMapping(path = "/project", method = RequestMethod.PUT)
    public ResponseEntity<?> manejadorPutRecursoProyecto(@RequestBody Project p){
        try {
            pj.agregarProyecto(p);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception ex) {
            Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(ex.getMessage(),HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(path = "/projects/{idProyecto}", method = RequestMethod.DELETE)
    public ResponseEntity<?> projectDeleteRecursoProyecto(@PathVariable Integer idProyecto){
    try {        
        return new ResponseEntity<>(pj.eliminarProyecto(idProyecto),HttpStatus.ACCEPTED);
    } catch (Exception ex) {
        Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
        return new ResponseEntity<>("No elimino proyecto!",HttpStatus.NOT_FOUND);
    }   
    }
    
    @RequestMapping(path = "/projects/activity/{projectid}/{activityid}", method = RequestMethod.GET)
    public ResponseEntity<?> projectGetRecursoActividadProyecto(@PathVariable Integer projectid, @PathVariable Integer activityid){
    try {
        
        return new ResponseEntity<>(pj.getActividadProyecto(activityid, projectid),HttpStatus.ACCEPTED);
    } catch (Exception ex) {
        Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
        return new ResponseEntity<>("No encontro Datos!",HttpStatus.NOT_FOUND);
    }    
    }
    
    @RequestMapping(path = "/activity/{projectid}/status/{activityid}", method = RequestMethod.POST)
    public ResponseEntity<?> manejadorRecursoActividadEstado (@PathVariable Integer activityid, @PathVariable Integer projectid){
        try{
            
            return new ResponseEntity<>(pj.cambiarEstadoActividad(activityid, projectid),HttpStatus.ACCEPTED);
        } catch(Exception ex){
            Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(path = "/project/{projectid}/equipos", method = RequestMethod.GET)
    public ResponseEntity<?> manejadorRecursoProyectoEquipo (@PathVariable Integer projectid){
        try{
            
            return new ResponseEntity<>(pj.getEquipoProyecto(projectid),HttpStatus.ACCEPTED);
        } catch(Exception ex){
            Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    
    
    @RequestMapping(path = "/project/activity/actividades/{activityid}/{projectid}", method = RequestMethod.PUT)
    public ResponseEntity<?> manejadorPutRecursoActividadActualizada(@PathVariable Integer activityid, @PathVariable Integer projectid, @RequestBody Activity a){
        try {
            pj.updateActividadProyecto(projectid, a, activityid);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception ex) {
            Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(ex.getMessage(),HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(path = "/activity/status/{projectid}/{activityid}", method = RequestMethod.GET)
    public ResponseEntity<?> manejadorRecursoActividadStatus (@PathVariable Integer projectid, @PathVariable Integer activityid){
        try{
            
            return new ResponseEntity<>(pj.getStatusActivityProyecto(projectid, activityid),HttpStatus.ACCEPTED);
        } catch(Exception ex){
            Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
        
    @RequestMapping(path = "/project/{projectId}/activity/{activityId}", method = RequestMethod.DELETE)
    public ResponseEntity<?> manejadorRecursoActividadProyecto(@PathVariable Integer projectId, @PathVariable Integer activityId){
        try {       
         return new ResponseEntity<>(pj.eliminarActividadProyecto(projectId, activityId),HttpStatus.ACCEPTED);
        } catch (Exception ex) {
             Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>("No elimino actividad!",HttpStatus.NOT_FOUND);
        }   
    }
    
    
}
