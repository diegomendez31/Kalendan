/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.kalendan.controllers;

import edu.eci.arsw.kalendan.services.UserService;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * 
 */

@RestController
@RequestMapping(value = "/kalendan/user/")
public class UserResourceController {
    
    @Autowired
    UserService us;
    
    @RequestMapping(path = "/equipos", method = RequestMethod.GET)
    public ResponseEntity<?> teamGetMiembros(){
        try{
            return new ResponseEntity<>(us.getUsuarios(),HttpStatus.ACCEPTED);
        } catch(Exception ex){
            Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(path = "/login/{nomUser}/{password}", method = RequestMethod.GET)
    public ResponseEntity<?> getLoginUsuario(@PathVariable String nomUser, @PathVariable String password) {
        try {
            return new ResponseEntity<>(us.login(nomUser, password), HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>("No encontro Datos!", HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<?> getRecursoUsuario() {
        try {
            return new ResponseEntity<>(us.getUsuarios(), HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>("No encontro Datos!", HttpStatus.NOT_FOUND);
        }
    }
    
    @RequestMapping(path = "/{nomUser}",method = RequestMethod.GET)
    public ResponseEntity<?> getRecursoUsuarioNombre(@PathVariable String nomUser) {
        try {
            return new ResponseEntity<>(us.getUserByNom(nomUser), HttpStatus.ACCEPTED);
        } catch (Exception ex) {
            Logger.getLogger(ProjectResourceController.class.getName()).log(Level.SEVERE, null, ex);
            return new ResponseEntity<>("No encontro Datos!", HttpStatus.NOT_FOUND);
        }
    }
    
}

