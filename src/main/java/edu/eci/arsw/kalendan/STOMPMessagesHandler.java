

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping(value = "/app")
public class STOMPMessagesHandler {

    @Autowired
    SimpMessagingTemplate msgt;
    
   // ArrayList<Point> puntos = new ArrayList<Point>();

    @MessageMapping("/cupdate.{projectid}")    
    public void cambiarFechaActividad(Date d, @PathVariable int projectid) throws Exception {
        System.out.println("Nuevo fecha recibido en el servidor!: "+d);
        msgt.convertAndSend("/topic/wupdate."+projectid, d);
    }
    
   
}
