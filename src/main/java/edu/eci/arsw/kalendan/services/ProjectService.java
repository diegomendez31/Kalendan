
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.kalendan.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import edu.eci.arsw.kalendan.model.Activity;

import edu.eci.arsw.kalendan.model.Project;
import edu.eci.arsw.kalendan.model.User;
import edu.eci.arsw.kalendan.repositories.REDISActivitiesRepository;
import edu.eci.arsw.kalendan.repositories.REDISProjectsRepository;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Diego
 */
@Service
public class ProjectService {
    @Autowired
    REDISProjectsRepository repo;
    
    @Autowired
    REDISActivitiesRepository repoAct;
    
    public static List<Activity> actividades;
    public ArrayList<Project> proyectos;
    public int projectCounter = 0;
    
    
    public ProjectService() throws ParseException{
        actividades = new LinkedList<>();
        proyectos = new ArrayList<>();              
    }
    
    public List<Activity> getActividadesProject(Integer idproject, String nameUser) throws IOException{
       proyectos = repo.getProjectsREDIS(); 
       for(int i=0; i< proyectos.size();i++){
           if(proyectos.get(i).getProjectid()==idproject && proyectos.get(i).getPermisoEditar(nameUser)){
               return proyectos.get(i).getActividadesProject();
           }
       } 
        return null;
    }
    
    public String getNameProject(Integer projectId) throws IOException{
        return repo.getProject(projectId).getName();
    }
    
    public void agregarMiembro(String u, Integer projectId, String permiso) throws JsonProcessingException, IOException{
        proyectos = repo.getProjectsREDIS();
        for(int i=0; i< proyectos.size();i++){
           if(proyectos.get(i).getProjectid()==projectId ){
               proyectos.get(i).setPermiso(u,permiso);
               repo.updateProject(proyectos.get(i));
           }
       } 
    }
    
    public void registrarActividad(Activity a, int projectId, String nameUser) throws IOException{        
        proyectos = repo.getProjectsREDIS();
        a.setIdActivity(repoAct.getIdNuevaActividad());
        repoAct.updateActivity(a);
        for(int i=0; i< proyectos.size();i++){
           if(proyectos.get(i).getProjectid()==projectId ){
             proyectos.get(i).agregarActividad(a);
             repo.updateProject(proyectos.get(i));
           }
       } 
    }
    
  /* private void cargarProyectos() throws ParseException{
       Project p1 = new Project(1,"Proyecto X", "diego");
       Project p2 = new Project(2,"Proyecto Y", "diego");
       Project p3 = new Project(3,"Proyecto Z", "diego");
       projectCounter++;
       User memb1 = new User("Pacho", "juanfg77@gmail.com", "pacho6", "pacho6");
       User memb2 = new User("Diego", "diegomendez@gmail.com", "diego", "diego");
       User memb3 = new User("Maria", "mariapaula@gmail.com", "mariapaulabn76", "mariapaulabn76");
         
       SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

       Activity a1 = new Activity(1,"Activity 1",sdf.parse("2016-12-16"), "mariapaulabn76", "08:00", "Esta es la actividad 1");
       Activity a2 = new Activity(2,"Activity 2",sdf.parse("2016-12-17"), "diego", "14:00", "Esta es la actividad 2");
       Activity a3 = new Activity(3,"Activity 3",sdf.parse("2016-12-18"), "pacho6", "18:00" , "Esta es la actividad 3");
       Activity a4 = new Activity(4,"Activity 4",sdf.parse("2016-12-19"), "mariapaulabn76", "13:00", "Esta es la actividad 4");
       Activity a5 = new Activity(5,"Activity 5",sdf.parse("2016-12-24"), "diego", "10:00", "Esta es la actividad 5");

       List<Activity> actividades = new ArrayList<>();
       actividades.add(a1);
       actividades.add(a2);
       actividades.add(a3);
       actividades.add(a4);
       actividades.add(a5); 
       
       actStaticas= new ArrayList<>();
       actStaticas.add(a1);
       actStaticas.add(a2);
       actStaticas.add(a3);
       actStaticas.add(a4);
       actStaticas.add(a5);       
       
       if(p1.getPermisoEditar("diego")){
            p1.setActividades(actividades);            
            proyectos.add(p1);
            proyectos.add(p2);
            proyectos.add(p3);
       }       
    }
    */
    public List<Project> getProjectsUsuario(String nombreUsuario) throws IOException{
        List<Project> misProyectos = new ArrayList<>();
        proyectos = repo.getProjectsREDIS();
        for(int i=0; i<proyectos.size(); i++){
            if(proyectos.get(i).isMyProject(nombreUsuario)){
                misProyectos.add(proyectos.get(i));
            }
        }
        return misProyectos;
    }
    //************************************
    public void cambiarFechaActividad(Date d, int idproject, String nameUser, Integer idActividad) throws IOException{
        proyectos = repo.getProjectsREDIS();
        
        for(int i=0; i<proyectos.size(); i++){
            if(proyectos.get(i).getProjectid()==idproject){
                proyectos.get(i).cambiarFechaActividad(idActividad, d);
                repo.updateProject(proyectos.get(i));                
                repoAct.updateActivity(proyectos.get(i).getActivityById(idActividad));
            }
        }        
    }    
    
    public ArrayList<Activity> getUserDateActivities(String userName, Date date) throws IOException{
        ArrayList<Activity> userActivities= new ArrayList<>();
        actividades=repoAct.getActivities();
        
        for(int i=0;i<actividades.size();i++){                        
            if(actividades.get(i).getEncargado().equals(userName) && 0==actividades.get(i).getDate().compareTo(date)){
                userActivities.add(actividades.get(i));
            }
        }        
        return userActivities;
    }   
    
    public ArrayList<String> cargarUsers(int idProject) throws IOException{
        proyectos=repo.getProjectsREDIS();
        ArrayList<String> nomUsers= new ArrayList<>();
        
        for(int i=0;i<proyectos.size();i++){
            if(proyectos.get(i).getProjectid()==idProject){
                nomUsers = proyectos.get(i).getUsuariosEquipo();
                return nomUsers;
            }
        }
        return nomUsers;
    }
    
    public List<Project> getProyectos() throws IOException {
        proyectos=repo.getProjectsREDIS();
        return proyectos;
    }

    public void setProyectos(List<Project> proyectos) {
        //repo.UpdateProject(pr);
        this.proyectos = (ArrayList<Project>) proyectos;
    }

    public int getProjectCounter() {
        return projectCounter;
    }

    public void setProjectCounter(int projectCounter) {
        this.projectCounter = projectCounter;
    }
    
    public void agregarProyecto(Project p) throws JsonProcessingException, IOException{        
        int nuevaId=repo.getIdNuevaProyecto();
        p.setProjectid(nuevaId);
        repo.updateProject(p);
    }
    
    public boolean eliminarProyecto(int projectid) throws IOException{
        repo.eliminarProject(repo.getProject(projectid));
        //proyectos.remove(projectid);
        return true;
    }
    
    public Activity getActividadProyecto(int activityId, int projectid) throws IOException{
        return repo.getProject(projectid).getActivityById(activityId);
    }
    
    public boolean cambiarEstadoActividad(int activityId, int projectId) throws IOException{
        
        Activity ac= repoAct.getActivity(activityId);
        Project pr=repo.getProject(projectId);
        ac.setEstado(!ac.isEstado());
        repoAct.updateActivity(ac);
        pr.cambiarEstadoActividad(activityId);
        repo.updateProject(pr);
        
        return true;
    }
  
    public List<String> getEquipoProyecto(int projectId) throws IOException{
        return repo.getProject(projectId).getUsuariosEquipo();
    }
    
    public void updateActividadProyecto(int projectId, Activity a, int activityId) throws JsonProcessingException, IOException{
        a.setIdActivity(activityId);
        repoAct.updateActivity(a);
        Project pr = repo.getProject(projectId);
        List<Activity> actividadesProyecto = pr.getActividadesProject();
        for(int i=0;i<actividadesProyecto.size();i++){
            if(actividadesProyecto.get(i).getIdActivity() == activityId){
                actividadesProyecto.remove(i);
                break;
            }
        }
        pr.getActividadesProject().add(a);
        repo.updateProject(pr);
       
    }
    
    public boolean getStatusActivityProyecto(int projectId, int activityId) throws IOException{
        return repo.getProject(projectId).getStatusActividad(activityId);
    }
    
    public boolean eliminarActividadProyecto(int projectId, int activityId) throws IOException{
        Project pr = repo.getProject(projectId);
        List<Activity> actividadesProyecto = pr.getActividadesProject();
        for(int i=0;i<actividadesProyecto.size();i++){
            if(actividadesProyecto.get(i).getIdActivity() == activityId){
                repoAct.eliminarActivity(actividadesProyecto.get(i));
                actividadesProyecto.remove(i);
            }
        }
        repo.updateProject(pr);
        return true;
    }
}

