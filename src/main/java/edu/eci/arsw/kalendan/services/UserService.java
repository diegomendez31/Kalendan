

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.kalendan.services;

import edu.eci.arsw.kalendan.model.User;
import edu.eci.arsw.kalendan.repositories.REDISUserRepository;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Diego
 */

@Service
public class UserService {
    
    @Autowired
    REDISUserRepository repoUser;
    
    List<User> usuarios = null;
    
    
    public UserService(){
        usuarios = new ArrayList<>();
    }
    
    public boolean login(String userName, String pass) throws IOException{
       usuarios = repoUser.getUsers();
       for(int i=0; i<usuarios.size(); i++){
           
           if(usuarios.get(i).getUserName().equals(userName)){
               if(usuarios.get(i).getPassword().equals(pass)){
                   return true;
               }
           }
       }
       return false;
    }
    
    public List<User> getUsuarios() throws IOException{
        return repoUser.getUsers();
    } 
    
      public boolean getUserByNom(String nomUser) throws IOException{
        usuarios= repoUser.getUsers();  
        for(int i=0; i<usuarios.size(); i++){
            if(usuarios.get(i).getUserName().equals(nomUser)){
                return true;
            }
        }
        return false;
    }
    
    
}
