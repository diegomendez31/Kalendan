/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.eci.arsw.kalendan.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import edu.eci.arsw.kalendan.model.User;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import util.JedisUtil;

/**
 *
 * @author Paula
 */
@Service
public class REDISUserRepository {
    
    public REDISUserRepository(){
        
    }
    
    public ArrayList<User> getUsers() throws IOException{
        ArrayList<User> users=new ArrayList<>();
        Jedis jedis = JedisUtil.getPool().getResource();
        ObjectMapper mapper = new ObjectMapper();
        Map<String,String>usersHash=jedis.hgetAll("users");
        
        for(Map.Entry<String, String>userJson: usersHash.entrySet()){
            users.add( mapper.readValue(userJson.getValue(), User.class));              
        }
        
        jedis.close();
        return users;
    }
}
