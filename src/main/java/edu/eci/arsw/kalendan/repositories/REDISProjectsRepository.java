/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.kalendan.repositories;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.eci.arsw.kalendan.model.Project;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import util.JedisUtil;

/**
 *
 * @author Paula
 */
@Service
public class REDISProjectsRepository {
    
    public REDISProjectsRepository(){
        
    }
    
    public ArrayList<Project> getProjectsREDIS()throws IOException{
        Jedis jedis = JedisUtil.getPool().getResource();
        ObjectMapper mapper = new ObjectMapper();
        ArrayList<Project> projects= new ArrayList<>();
        Map<String,String>projectsHash=jedis.hgetAll("proyectos");
        for(Map.Entry<String, String>projectJson: projectsHash.entrySet()){
            projects.add( mapper.readValue(projectJson.getValue(), Project.class));              
        }
        
        jedis.close();
        return projects;
    }
    
    public Project getProject(int idProject) throws IOException{
        Jedis jedis = JedisUtil.getPool().getResource();
        ObjectMapper mapper = new ObjectMapper();
        String proyecto= jedis.hget("proyectos", idProject+"");        
        Project pr= mapper.readValue(proyecto, Project.class);               
        
        jedis.close();
        return pr;
    }
    
    public void updateProject(Project pr) throws JsonProcessingException{
        Jedis jedis = JedisUtil.getPool().getResource();
        ObjectMapper mapper = new ObjectMapper();
        jedis.hset("proyectos", pr.getProjectid()+"", mapper.writeValueAsString(pr));
        jedis.close();
    }
    
    public void eliminarProject(Project p){
        Jedis jedis = JedisUtil.getPool().getResource();
        ObjectMapper mapper = new ObjectMapper();
        jedis.hdel("proyectos", p.getProjectid()+"");
        jedis.close();
    }
    
    public Integer getIdNuevaProyecto(){
        Jedis jedis = JedisUtil.getPool().getResource();
        Integer idActual = Integer.parseInt(jedis.get("contador_proyectos"));
        jedis.set("contador_proyectos", (idActual+1)+"");
        
        jedis.close();
        return idActual+1;
    }
    
}
