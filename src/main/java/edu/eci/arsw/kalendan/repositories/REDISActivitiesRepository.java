/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.kalendan.repositories;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.eci.arsw.kalendan.model.Activity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import util.JedisUtil;

/**
 *
 * @author Paula
 */
@Service
public class REDISActivitiesRepository {
    
    public REDISActivitiesRepository(){
        
    }
    
    public ArrayList<Activity> getActivities() throws IOException{
        Jedis jedis = JedisUtil.getPool().getResource();
        ObjectMapper mapper = new ObjectMapper();
        ArrayList<Activity> activities= new ArrayList<>();
        Map<String,String>activitiesHash=jedis.hgetAll("activities");
        
        for(Map.Entry<String, String>activityJson: activitiesHash.entrySet()){
            activities.add( mapper.readValue(activityJson.getValue(), Activity.class));              
        }
        
        jedis.close();
        return activities;
    }
    
    public Activity getActivity(int idAct) throws IOException{
        Jedis jedis = JedisUtil.getPool().getResource();
        ObjectMapper mapper = new ObjectMapper();
        String activity=jedis.hget("activities", idAct+"");
        Activity ac = mapper.readValue(activity, Activity.class);
        
        jedis.close();
        return ac;
    }
    
    public Integer getIdNuevaActividad(){
        Jedis jedis = JedisUtil.getPool().getResource();
        Integer idActual = Integer.parseInt(jedis.get("contador_actividades"));
        jedis.set("contador_actividades", (idActual+1)+"");
        
        jedis.close();
        return idActual+1;
    }
    
       public void updateActivity(Activity act) throws JsonProcessingException{
        Jedis jedis = JedisUtil.getPool().getResource();
        ObjectMapper mapper = new ObjectMapper();
        jedis.hset("activities", act.getIdActivity()+"", mapper.writeValueAsString(act));
        jedis.close();
    }
       
    public void eliminarActivity(Activity act){
        Jedis jedis = JedisUtil.getPool().getResource();
        ObjectMapper mapper = new ObjectMapper();
        jedis.hdel("activities", act.getIdActivity()+"");
        jedis.close();
    }
}
