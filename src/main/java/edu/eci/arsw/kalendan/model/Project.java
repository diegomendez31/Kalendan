
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.kalendan.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Diego
 */
public class Project {
    
    
    public static String permisoAdmin = "ADMIN";
    public static String permisoColaborador = "COLA";
    public static String permisoInvitado = "INV";
    private int projectid; 
    private List<Activity> actividades = null;
    private String name;
    private String creator;
    private HashMap<String, String> equipo = new  HashMap<> ();

    public HashMap<String, String> getEquipo() {
        return equipo;
    }

    public void setEquipo(HashMap<String, String> equipo) {
        this.equipo = equipo;
    }
   
    
    public Project(int projectid, String name, String creator){
        this.projectid = projectid;
        this.actividades = new ArrayList<>();
        this.name = name;
        this.creator = creator;

        equipo.put(this.creator, permisoAdmin);
      
        
        
    }
    
    public Project(){
        this.actividades = new ArrayList<>();
    }
    
    public boolean isMyProject(String nombreUsuario){
        if(equipo.containsKey(nombreUsuario)){
  
            return true;
        }else{
            return false;
        }
    }
    
    public void agregarActividad(Activity a){
        actividades.add(a);
    }
    
    public boolean getPermisoEditar(String userName){
        if(equipo.containsKey(userName)){
            if(equipo.get(userName).equals(permisoAdmin) || equipo.get(userName).equals(permisoColaborador)){
                return true;
            }
        }
        return false;
    }
    
    public void setActividades(List<Activity> actividades) {
       this.actividades = actividades;
    }
    
    public void cambiarFechaActividad(Integer idActividad, Date d){
        for(int i=0; i<actividades.size();i++){
            if(actividades.get(i).getIdActivity() == idActividad){
                d.setDate(d.getDate());
                actividades.get(i).setDate(d);
                break;
            }
        }
    }
    
    public ArrayList<String> getUsuariosEquipo(){

       return  new ArrayList<String>(equipo.keySet());             
    }
    
    public List<Activity> getActividadesProject(){
        return actividades;
    }

   

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
        equipo.put(this.creator, permisoAdmin);
    }    

    public int getProjectid() {
        return projectid;
    }

    public void setProjectid(int projectid) {
        this.projectid = projectid;
    }

    public HashMap<String, String> getPermisos() {
        return equipo;
    }

    public void setPermisos(HashMap<String, String> permisos) {
        this.equipo = permisos;
    }
    
     public void setPermiso(String nombre, String permiso) {
        this.equipo.put(nombre, permiso);
    }
    
    public Activity getActivityById(int activityId){
        for(int i=0; i<actividades.size(); i++){
            if(actividades.get(i).getIdActivity() == activityId)
                return actividades.get(i);
        }
        return null;
    }
    
    public Activity getActivityByName(String actNom){
        for(int i=0; i<actividades.size();i++){
            if(actividades.get(i).getName().equals(actNom)){
                return actividades.get(i);
            }
            
        }
        return null;
    }

    
    public void cambiarEstadoActividad(int activityId) {
        for(int i=0; i<actividades.size(); i++){
            if(actividades.get(i).getIdActivity() == activityId){
                actividades.get(i).setEstado(true);
                break;
            }
            
        }
        
    }
    
    public void updateActividad(Activity a, int activityId){
        a.setIdActivity(activityId+1);
        actividades.add(activityId+1, a);
        actividades.remove(activityId);
    }

    public boolean getStatusActividad(int activityId) {
        return actividades.get(activityId).isEstado();
    }
    
    public boolean eliminarActividad(int activityId){
        actividades.remove(activityId);
        return true;
    }
}

   