/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.kalendan.model;

/**
 *
 * @author Diego
 */
public class User {
    
    private String name;
    private String mail;
    private String userName;
    private String password;
    
    public User(String nam, String email, String user, String pass){
        this.name = nam;
        this.mail = email;
        this.userName = user;
        this.password = pass;
    }
    
    public User(){
        
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
