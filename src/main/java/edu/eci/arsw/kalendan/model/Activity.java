/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.eci.arsw.kalendan.model;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 *
 * @author Diego
 */
public class Activity {
    

    private int idActivity;

    private String encargado;
    private String descripcion;
    private String hora;
    private String name;
    private Date date = null;
    private Number day;
    private Number month;
    private Number year;
    private Calendar c;
    private boolean estado;

    
    public Activity(int idActivity, String name, Date date, String encdo, String hora, String descripcion){

   
        this.idActivity = idActivity;
        this.name = name;
        this.setDate(date);
        this.idActivity = idActivity;
        this.encargado = encdo;
        this.hora=hora;
        this.descripcion = descripcion;
        this.estado = false;
    }
    
    public void agregarActividad(Activity a){        
        this.estado = false;
    }
    
    public Activity(){
        
    }
    
    public Date getDate() {
        return date;
    }
        
    public void setDate(Date date) {
        this.date = date;
        c = Calendar.getInstance();
         c.setTime(this.date);
        c.setTimeZone(TimeZone.getTimeZone("COT"));
        this.day = c.get(Calendar.DAY_OF_MONTH);
        this.month = c.get(Calendar.MONTH)+1;
        this.year = c.get(Calendar.YEAR);
        
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Number getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public Number getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public Number getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
    
    public String getEncargado() {
        return encargado;
    }

    public void setEncargado(String encargado) {
        this.encargado = encargado;
    }
    
    public int getIdActivity() {
        return idActivity;
    }

    public void setIdActivity(int idActivity) {
        this.idActivity = idActivity;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    

}
