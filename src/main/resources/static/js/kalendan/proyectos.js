/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

cargarProyectos = function(){
    return $.ajax({
        type: 'GET',
        url: "/kalendan/projects/calendar/"+localStorage.getItem("nombreUsuario"),
        dataType: "json",
        success: function(data){
            var datos;   
            for(i=0; i<data.length; i++){
                //alert(data[i].name)
                $("#proyectosimg").append("<td><img id='"+data[i].projectid+"' src='images/calendar.png' onClick='seleccionarProyecto(this.id)' alt='calendar'"+i+"' height='150' width='150'></td>");
                $("#proyectosnom").append("<td>"+data[i].name+"</td>");
                $("#proyectosdel").append("<td><img id='delete"+data[i].projectid+"' src='images/delete.png' onClick='eliminarProyecto(this.id)' alt='calendar'"+i+"' height='30' width='30'></td>");
            }
                $("#proyectosimg").append("<td><img id='calendarplus' src='images/calendarplus.png' onClick='nuevoProyecto()' alt='calendarplus' height='150' width='150'></td>");
                $("#proyectosnom").append("<td>Agregar Proyecto</td>");
            }
    });
}
    
eliminarProyecto = function(deleteid){
     var idPro = parseInt(deleteid.slice(-1));
     return $.ajax({
        type: 'DELETE',
        url: "/kalendan/projects/"+idPro,
        dataType: "json",
        success: function(data){
                $("#proyectosimg").html("");
                $("#proyectosnom").html("");
                $("#proyectosdel").html("");
                cargarProyectos();
        }
     });
}

seleccionarProyecto = function(id){
                localStorage.setItem("projectId", id);
                var pagina="calendarioPrincipal.html";
                location.href=pagina;
}

nuevoProyecto = function(){
                
                var pagina="crearProyecto.html";
                location.href=pagina;
}

$(document).ready(
        function() {
    
        cargarProyectos();
		
	}
);
