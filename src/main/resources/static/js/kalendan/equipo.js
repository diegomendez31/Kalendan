/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var Id=1;
var permiso;
var nomU;
var nombreProyecto;
var usur;
var miem=[];

function getMiembros(){
    
     var promise = $.ajax({
        url: "/kalendan/project/"+localStorage.getItem("projectId")+"/equipos",
        type: 'GET',
        contentType: "application/json",
        dataType: "json"
    });
        promise.done( function(data){
            miem = data;
            $("#miembros").html(" ");
            mostrarMiembros();
        });
    return promise;
}

function postMiembro(){
    return $.ajax({        
            url: "/kalendan/equipos/"+localStorage.getItem("projectId")+"/"+permiso,
            type: 'POST',
            data: document.getElementById("userT").value,
            contentType: "application/json",
            success: function(){
                $("#miembros").html(" ");
                getMiembros();
                mostrarMiembros();
            }    
        });
}

function convertMiembros(){
  return $.ajax({
        type: 'GET',
        url: "/kalendan/user/"+document.getElementById("userT").value,
        dataType: "json",
        success: function(data){
            if(data === true){
                postMiembro();
                getMiembros();
            }
        }
    });
}

function nombreEquipo(){
 
    var promise = $.ajax({
        url: "/kalendan/projects/"+localStorage.getItem("projectId"),
        type: "GET",
        contentType: "application/json",
        dataType: "json"      
    });
        promise.done(function(data){
            document.getElementById("proy").innerHTML = data.toString();
        });
    return promise;           
}

function mostrarMiembros(){
       for(i=0; i<miem.length; i++){
           $("#miembros").append("<tr><td>"+miem[i]+"</td></tr>");
        }
}

cambiarPermiso = function (perm){
    permiso = perm;
    console.info(perm);
};

$(document).ready( 
    function(){
        $("#miembros").html("");
        nombreEquipo();
        getMiembros();
});