/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var horaValido;
var fecha;
var fecha2;
var nombre;
var nombreUsuario="diego";
var projectId=1;
var actividades=[];
var horaAct;
var equipoId=1;
var usuarios=[];
var encargadoAct;
var description;
var stompClient = null;


function actualizarActividad(){
    fecha2 = new Date(fecha);
    return $.ajax({        
                        url: "/kalendan/project/activity/actividades/"+localStorage.getItem("tareaId")+"/"+localStorage.getItem("projectId"),
                        type: 'PUT',
                        data: activityToJson(),
                        contentType: "application/json",
                        success: function(data, textStatus, jqXHR){
                            //alert('Actividad movida');
                            //console.info(data);
                            //stompClient.send("/topic/cupdate."+localStorage.getItem("projectId"), {}, JSON.stringify(fecha));
                            stompClient.send("/topic/cupdate."+localStorage.getItem("projectId"), {}, JSON.stringify(fecha));
                            var pagina="calendarioPrincipal.html";
                            location.href=pagina;
                        }    
                     });
    
}

function connect() {
    var socket = new SockJS('/stompendpoint');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        
        console.log('Connected: ' + frame);
 
        
    });
    
}

function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function activityToJson(){
    var index=document.getElementById("encargados").selectedIndex;
   
    return JSON.stringify({
    "idActividad": localStorage.getItem("tareaId"), 
    "name": nombre,
    "date": fecha2.getTime(),
    "encargado": usuarios[index],
    "hora": horaAct,
    "descripcion": description
    });
}

function get_EncargadosPorEquipo(){
    var promise= $.ajax({
        type: 'GET',
        url: "/kalendan/equipos/"+equipoId,
        dataType: "json",
        contentType: "application/json"
    });
        promise.done( function(data){
            usuarios=data;
            console.info(data);
            var optionV;
            console.info( document.getElementById("encargados"));
            //$('#divEncargados').empty();
           // $('#encargados').empty();
            
            for(var i=0; i<data.length; i++){
                optionV=document.createElement("option");
                optionV.value= data[i];
                optionV.text=  data[i];
                document.getElementById("encargados").appendChild(optionV);
            }
            encargadoAct=data[0];
            
        });   
     return promise;        
}





function getActivityName(hora){
    if(actividades){
        for(var i=0 ; i<actividades.length;i++){
            console.info("hora de la actividad "+ actividades[i].hora );
            console.info("hora por parametro ");
            console.info(hora.textContent);
            if(actividades[i].hora === hora.textContent)
                return actividades[i].name;
        }
    }
    return "           ";
}
function getHoraTabla(i){
    
    var j = zeroFill(i, 2);
    return  j +":00";
}

function getHoras(){
    var selection = "";
    var i = 0;
    for(var i = 0; i < 24; i++)
    {
        var j = zeroFill(i, 2);
        selection += "<option value='"+ j +":00'>"+ j + ":00" + "</option>";
    }
    document.getElementById("hours").innerHTML=selection;
}

function zeroFill( number, width ){
    width -= number.toString().length;
    if ( width > 0 )
    {
      return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
    }
    return number + ""; // devuelve un String
}

function cambiarNombreActividad(nombreF){    
    nombre =nombreF;
    console.info(nombre);   
};

function cambiarHora(hora){
    horaAct =hora;
    console.info(horaAct);
}

function cambiarEncargado(encargado){
    encargadoAct =encargado;
    console.info(encargadoAct);
}

function cambiarFecha(date){
    fecha = date.toString();
    fecha2 = date;
    console.info(fecha);
    
}

function cambiarDescription(dec){
    description=dec;
    console.info(description);
}

function cambiarEstadoActivity(){
   return $.ajax({        
                url: "/kalendan/activity/"+localStorage.getItem("projectId")+"/status/"+localStorage.getItem("tareaId"),
                type: 'POST',
                data: true,
                contentType: "application/json",
                success: function(data, textStatus, jqXHR){
                            //alert('Actividad movida');
                            //console.info(data);
                            //stompClient.send("/topic/cupdate."+localStorage.getItem("projectId"), {}, JSON.stringify(fecha));
                            stompClient.send("/topic/cupdate."+localStorage.getItem("projectId"), {}, JSON.stringify(fecha));
                            var pagina="calendarioPrincipal.html";
                            location.href=pagina;
                      }    
                });
}

function eliminarActividad(){

     return $.ajax({
        type: 'DELETE',
        url: "/kalendan/project/"+localStorage.getItem("projectId")+"/activity/"+localStorage.getItem("tareaId"),
        dataType: "json",
        success: function(data){
            stompClient.send("/topic/cupdate."+localStorage.getItem("projectId"), {}, JSON.stringify(fecha));
            var pagina="calendarioPrincipal.html";
            location.href=pagina;    
        }
     });
}

function cargarDatosTarea(){
    return $.ajax({
        type: 'GET',
        url: "/kalendan/projects/activity/"+localStorage.getItem("projectId")+"/"+localStorage.getItem("tareaId"),
        dataType: "json",
        success: function(data){
            ele = document.getElementById("tituloTarea");
            ele.innerHTML = ""+data.name;
            d = new Date(data.date);
            document.getElementById("tarea_name").value = data.name;
            nombre = data.name;
            document.getElementById("encargados").value = data.encargado;
            encargadoAct = data.encargado;
            document.getElementById("fecha_usuario").value = d.getFullYear()+"-"+d.getMonth()+"-"+d.getDate();
            fecha = d.toString();
            fecha2 = d;
            document.getElementById("hours").value = data.hora;
            horaAct = data.hora;
            document.getElementById("description").value = data.descripcion;
            description = data.descripcion;
            $("#botones").append("<td><button id='botonFinalizar' class='btn btn-primary' onclick='cambiarEstadoActivity()' >Finalizar Tarea</button></td>");
            $("#botones").append("<td><button id='botonActualizar' class='btn btn-primary' onclick='actualizarActividad()' >Editar Tarea</button></td>");
            $("#botones").append("<td><button id='botonEliminar' class='btn btn-primary' onclick='eliminarActividad()' >Eliminar Tarea</button></td>");
        }
    });
}

$(document).ready(
    function(){
        connect();
        console.info('connecting to websockets');
        getHoras();
        get_EncargadosPorEquipo();
        cargarDatosTarea();
        
    });

