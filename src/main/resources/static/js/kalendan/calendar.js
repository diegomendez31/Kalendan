actividades = [];
actividadesCalendario = [];
var eventos;
var contadorAc = 1;

var act;
var fecha;

var stompClient = null;




cambiarFecha = function(dateBefore, dayAfter, idActivity){
    fecha = new Date(dateBefore);
    fecha.setDate(fecha.getDate()+dayAfter)
    //console.info("Lo moviii    " + fecha);
    return $.ajax({        
            url: "/kalendan/actividades/calendario/"+localStorage.getItem("projectId")+"/"+localStorage.getItem("nombreUsuario")+"/"+idActivity,
            type: 'PUT',
            data: JSON.stringify(fecha),
            contentType: "application/json",
            success: function(data, textStatus, jqXHR){
                //alert('Actividad movida');
                //console.info(data);
                stompClient.send("/topic/cupdate."+localStorage.getItem("projectId"), {}, JSON.stringify(fecha));
            }    
        });
}

function activityToJson(){
    var index=document.getElementById("encargados").selectedIndex;
    return JSON.stringify({
    "name": nombre,
    "date": fecha.getTime(),
    //"day": fecha2.getDay(),
   // "month": fecha2.getMonth(),
   // "year": fecha2.getFullYear(),
   // "encdo": usuarios[index],
    //"hora": horaAct
    });
}

getActividadesCalendario = function(){
    return $.ajax({
        type: 'GET',
        url: "/kalendan/projects/"+localStorage.getItem("projectId")+"/"+localStorage.getItem("nombreUsuario"),
        dataType: "json",
        success: function(data){
            var datos;   
       
            actividades = [];
            actividadesCalendario = [];
            actividades = data;
           var nombre;
            var day;
            var month;
            var year;
            var i;
            for(i=0; i<actividades.length; i++){
                act = null;
                datos = new Actividad();
                datos = actividades[i];
                //var fechaa = new Date(datos.date);
                //alert(fechaa.getDate());
                nombre = datos.name;
                //day = datos.day;
                //alert(datos.day);
                //month = datos.month;
                //year = datos.year;
                fecha = new Date();
               
                fecha.setMonth(datos.month-1);
                fecha.setFullYear(datos.year);
                 fecha.setDate(datos.day);
                //alert(datos.name + fecha.getDay() + fecha.getMonth() + fecha.getFullYear());
                act = new Activity(datos.idActivity,nombre,fecha);
                actividadesCalendario.push(act);
                //alert(act.day);
            }
            $('#calendar').fullCalendar({
			draggable: true,
			events: actividadesCalendario
		});
                cargarFinalizados();
            }
    });
    
}

actividadesToEvents = function(){
    var nombre;
    var day;
    var month;
    var year;
    for(i=0; i<actividades; i++){
        nombre = actividades[i].name;
        day = actividades[i].day;
        month = actividades[i].month;
        year = actividades[i].year;
        date = new Date(year, month, day);
        act = new Activity(i,nombre,date);
        actividadesCalendario.push(act);
    }
   
}
actualizarActividades = function(){
 
	
		$('#calendar').fullCalendar({
			draggable: true,
			events: actividadesCalendario
		});
}

cargarCalendario = function (){
    
    getActividadesCalendario();
           // then(actividadesToEvents).then(actualizarActividades);
    
}

editarTarea = function(idTarea){
    localStorage.setItem("tareaId", idTarea);
    localStorage.setItem("editarTarea", true);
    var pagina="Actividad.html";
    location.href=pagina;
}

function connect() {
    var socket = new SockJS('/stompendpoint');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        
        console.log('Connected: ' + frame);
        stompClient.subscribe('/topic/cupdate.'+localStorage.getItem("projectId"), function (data) {
           caracter = null;
           fecha = JSON.parse(data.body);
           document.getElementById("calendar").innerHTML="";
          // alert("Voy a cargar nuevo calendario del project "+projectId+" con fehca "+fecha);
           
           cargarCalendario();
        });
        
    });
    
}

function cargarFinalizados(){
    
    for(j=0; j<actividades.length; j++){
        
            if(actividades[j].estado == true){
                 var pos = actividades[j].idActivity;
                document.getElementById(""+pos).src='images/check.png'
                document.getElementById(""+pos).onclick = function () {
                };
            }         
        }
}

function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}


$(document).ready(
        function() {
    
	/*var d = new Date();
		var y = d.getFullYear();
		var m = d.getMonth();
                var act = new Activity(1, "Hola", new Date(y, m, 20, 9, 0) );
                var act1 = new Activity(2, "aaaaaaaaaaaaaaaa", new Date(y, m, 5, 9, 0) );
                actividadesCalendario.push(act);
                actividadesCalendario.push(act1);
                alert(act1.start);
		$('#calendar').fullCalendar({
			draggable: true,
			events:actividadesCalendario
		});*/
        connect();
      
        console.info('connecting to websockets');
        localStorage.setItem("editarTarea", false);
        cargarCalendario();
        
	}
);

function Activity(id, nombre , date){
    this.id = id;
    this.title=nombre;
    this.start=date;
}

function Actividad(){
    this.name = "name";
    this.date = 0;
    this.day = 0;
    this.month = 0;
    this.year = 0;
}
