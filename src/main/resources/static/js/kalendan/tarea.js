/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var horaValido;
var fecha;
var fecha2;
var nombre;
var nombreUsuario;
var projectId;
var actividades=[];
var horaAct;
var equipoId;
var usuarios=[];
var encargadoAct;
var description;
var stompClient= null;

function put_actividadNueva(){
    fecha2 = new Date(fecha);
    return $.ajax({        
            url: "/kalendan/actividades/"+localStorage.getItem("projectId")+"/"+localStorage.getItem("nombreUsuario"),
            type: 'PUT',
            data: activityToJson(),
            contentType: "application/json",            
            success: function(data, textStatus, jqXHR){
                
                stompClient.send("/topic/cupdate."+localStorage.getItem("projectId"), {}, JSON.stringify(fecha));
                var pagina="calendarioPrincipal.html";
                location.href=pagina;
            }
    });
}

function checkDuplicateActivities(){
    var duplicatedActivities=false;
    for(var i=0; i<actividades.length;i++){
        if(actividades[i].hora.trim()===horaAct.trim()){
            duplicatedActivities=true;
            alert("ERROR: Ya hay una actvidad a esa hora. Por favor cambie la hora");            
        }       
    }
    if(!duplicatedActivities)
    put_actividadNueva();
    
}

function connect() {
    var socket = new SockJS('/stompendpoint');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        
        console.log('Connected: ' + frame);
 
        
    });
    
}

function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function activityToJson(){
    var index=document.getElementById("encargados").selectedIndex;
    
    console.info("nombre "+nombre);
    console.info("date "+fecha2.getTime());
    console.info("hora "+ horaAct);
    return JSON.stringify({
    "name": nombre,
    "date": fecha2.getTime(),
    "encargado": usuarios[index],
    "hora": horaAct,
    "descripcion": description
    });
}

function get_EncargadosPorEquipo(){
    var promise= $.ajax({
        type: 'GET',
        url: "/kalendan/equipos/"+localStorage.getItem("projectId"),
        dataType: "json",
        contentType: "application/json"
    });
        promise.done( function(data){
            usuarios=data;
            console.info(data);
            var optionV;
            console.info( document.getElementById("encargados"));
           // $('#divEncargados').empty();
           // $('#encargados').empty();
            
            for(var i=0; i<data.length; i++){
                optionV=document.createElement("option");
                optionV.value= data[i];
                optionV.text=  data[i];
                document.getElementById("encargados").appendChild(optionV);
            }
            encargadoAct=data[0];
            
        });   
     return promise;        
}

function get_UserActivitiesByDate(){
    var index=document.getElementById("encargados").selectedIndex;
    var promise= $.ajax({
        type: 'POST',
        url: "/kalendan/actividades/"+(new Date(fecha2).getTime()),
        data: usuarios[index],
        dataType: "json",
        contentType: "application/json"
    });
    promise.done( function(data){
        actividades=data;
        console.log(actividades);
        actualizar_tablaHoras();
    }); 
}

function actualizar_tablaHoras(){
    var table = document.getElementById("tablaHoras");
    var divTable =document.getElementById("divTablaHoras");
    $('#divTablaHoras').empty();
    $('#tablaHoras').empty();
    for(var i=0; i<24;i++){
        var tr = document.createElement("tr");
        var tupleHora = document.createElement("td");
        var tuplaNombre = document.createElement("td");
        var hora=getHoraTabla(i);
        console.info(getHoraTabla(i));
        var textHora = document.createTextNode(hora);
        var nombreActividad = getActivityName(textHora);
        var textNombre = document.createTextNode(nombreActividad);
        tupleHora.appendChild(textHora);
        tuplaNombre.appendChild(textNombre);        
        
        tr.appendChild(tupleHora);
        tr.appendChild(tuplaNombre);
        
        table.appendChild(tr);
        
        divTable.appendChild(table);
    }   
}

function getActivityName(hora){
    if(actividades){
        for(var i=0 ; i<actividades.length;i++){
            //console.info("hora de la actividad "+ actividades[i].hora );
            //console.info("hora por parametro ");
            //console.info(hora.textContent);
            if(actividades[i].hora === hora.textContent)
                return actividades[i].name;
        }
    }
    return "           ";
}
function getHoraTabla(i){
    
    var j = zeroFill(i, 2);
    return  j +":00";
}

function getHoras(){
    var selection = "";
    var i = 0;
    for(var i = 0; i < 24; i++)
    {
        var j = zeroFill(i, 2);
        selection += "<option value='"+ j +":00'>"+ j + ":00" + "</option>";
    }
    document.getElementById("hours").innerHTML=selection;
}

function zeroFill( number, width ){
    width -= number.toString().length;
    if ( width > 0 )
    {
      return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
    }
    return number + ""; // devuelve un String
}

function cambiarNombreActividad(nombreF){    
    nombre =nombreF;
    console.info(nombre);   
};

function cambiarHora(hora){
    horaAct =hora;
    console.info(horaAct);
}

function cambiarEncargado(encargado){
    encargadoAct =encargado;
    console.info(encargadoAct);
}

function cambiarFecha(date){
    fecha = date;
    fecha2 = date;
    console.info("fecha como esta "+ date.toString());
    console.info("imprimiendo fecha seleccionada");
    console.info(new Date(date).getTime());
    get_UserActivitiesByDate();
}

function cambiarDescription(dec){
    description=dec;
    console.info(description);
}

function cambiarEstadoActivity(){
   
}

function cargarDatosTarea(){
    return $.ajax({
        type: 'GET',
        url: "/kalendan/projects/activity/"+localStorage.getItem("projectId")+"/"+localStorage.getItem("tareaId"),
        dataType: "json",
        success: function(data){
            ele = document.getElementById("tituloTarea");
            ele.innerHTML = ""+data.name;
            d = new Date(data.date);
            document.getElementById("tarea_name").value = data.name;
            
            document.getElementById("fecha_usuario").value = d.getFullYear()+"-"+d.getMonth()+"-"+d.getDate();
            document.getElementById("hours").value = data.hora;
            document.getElementById("description").value = data.descripcion;
            $("#botones").append("<button id='botonFinalizar' class='btn btn-primary' onclick='cambiarEstadoActivity()' >Agregar Tarea</button>");
        }
    });
}

$(document).ready(
    function(){
        connect();
        console.info('connecting to websockets');
        getHoras();
        get_EncargadosPorEquipo();
        
    });