6810525cn

PROYECTOS REDIS:
HSET proyectos 1 '{"projectid": 1,"name": "project1","creator": "diego","equipo": {"diego": "ADMIN"},"actividades": [{"idActivity": 1,"name": "actividad 1","date": 1481846400000,"encargado": "diego","hora": "05:00","descripcion": "descripcion 1"}]}'
HSET proyectos 2 '{"projectid": 2,"name": "project2","creator": "diego","equipo": {"diego": "ADMIN"},"actividades": [{"idActivity": 2,"name": "actividad 2","date": 1480907076053,"encargado": "diego","hora": "04:00","descripcion": "descripcion X"}]}'

ACTIVIDADES REDIS
HSET activities 1 '{"idActivity": 1,"name": "actividad 1","date": 1481846400000,"encargado": "diego","hora": "05:00","descripcion": "descripcion 1"}'
HSET activities 2 '{"idActivity": 2,"name": "actividad 2","date": 1480907076053,"encargado": "diego","hora": "04:00","descripcion": "descripcion X"}'

USER REDIS
HSET users diego '{"name":"diego","mail":"diegomendez@gmail.com","userName":"diego","password":"diego"}'
HSET users paula '{"name":"paula","mail":"paula@gmail.com","userName":"paula","password":"paula"}'
HSET users pacho '{"name":"pacho","mail":"pacho@gmail.com","userName":"pacho","password":"pacho"}'

CONTADOR ACTIVIDADES
SET contador_actividades 2
CONTADOR USUARIOS
SET contador_usuarios 3
CONTADOR PROYECTOS
SET contador_proyectos 2
